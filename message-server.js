var express = require('express'),	
	http = require('http'),
	app = express(),
	server = http.createServer(app),
	io = require('socket.io').listen(server);

online = [];
users = [];
sockets = [];

// Routing

app.get("/js/messaging.js", function (req,res) {
	res.sendfile(__dirname + '/public/js/messaging.js');
});

app.get("/", function (req,res) {
	res.sendfile(__dirname + '/views/messaging.html');
});

app.get("/users", function (req,res) {
	res.send(users);
});

app.get("/online", function (req,res) {
	res.send(online);
});

server.listen(5001);

io.sockets.on('connection', function (socket) {
	currentUser = null;
	console.log(socket.handshake.address.address);
	for (i=0;i<users.length;i++) {
		user = users[i];
		if (user.ip == socket.handshake.address.address) {
			for (i=0;i<online.length;i++) {
				onlineUser = online[i];
				if (user == onlineUser) {
					currentUser = user;
					socket.emit('reloadUsers', { onlineUsers: online });
					console.log(online);
					return;
				}
			}
			online.push(user);
			currentUser = user;
			console.log(online);
			socket.emit('reloadUsers', { onlineUsers: online });
			return;
		}
	}
	socket.emit('pickName');

	socket.on('usernameResponse', function (data) {
		console.log(socket.handshake.address.address);
		sockets.push(socket);
		newUser = {
			username: data.username,
			ip: socket.handshake.address.address,
			socketIndex: sockets.length-1
		};
		users.push(newUser);
		online.push(newUser);
		currentUser = newUser;
		socket.emit('welcomeBack', { username: newUser.username });
		socket.emit('youAre', { user: newUser });
		io.sockets.emit('reloadUsers', { onlineUsers: online });
	});


	socket.on('disconnect', function (socket) {
		for (i=0;i<users.length;i++) {
			user = users[i];
			if (user == currentUser) {
				online.pop(user);
			}
		}
	});

	socket.on('chat', function (data) {
		console.log(data);
		for (i=0;i<online.length;i++) {
			if (data.to.username == online[i].username) {
				console.log("sending");
				sockets[online[i].socketIndex].emit('chat', {
					from: data.from,
					message: data.message
				});
				return;
			}
		}
		socket.emit('recipientNotFound');
	});
});