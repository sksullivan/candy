// Import Libraries

express = require('express');
crypto = require('crypto');
Facebook = require('fb');
request = require('request');
googleapis = require('googleapis');
sc = require('soundclouder');
Db = require('mongodb').Db,
Server = require('mongodb').Server;
db = new Db('candy', new Server('localhost', 27017), {safe:true});
nodemailer = require("nodemailer");
Imap = require('imap'),
util = require('util');

// Set up the object for our app
var app = express();

// In-memory data storage

// Get an object in ther server's memory to hold access tokens, other data we will get through authentication

app.prototype.data = null;
app.data = {
	authRedir: "/",
	facebook: {
		accessToken: null,
		refreshToken: null,
		user: null // Bit of info that identifies who authenticated
	},
	google: {
		accessToken: null,
		refreshToken: null,
		clientId: "208173405027-qptm09scplgho099tqorj2fmg41b75lt.apps.googleusercontent.com",
		clientSecret: "29HGLBJJoR_4h05mRRUv6UIZ",
		redirUrl: "http://localhost:5000/googleauth"
	},
	reddit: {
		accessToken: null,
		refreshToken: null,
		user: null,
		clientId: "vjfHG4sRVNC7uw", // These bits of info let reddit know that our app is legit
		clientSecret: "M1cl8Pz6wuxzIUb5_WkeQgstpLM",
		redirUrl: "http://localhost:5000/redditauth"
	},
	instagram: {
		accessToken: null,
		refreshToken: null,
		clientId: '3bd80df59b9448c1a985abd64cf27b4e',
		clientSecret: '4a933e2d4b054d92b723addc547408dc',
		redirUrl: 'http://localhost:5000/instagramauth'
	},
	soundcloud: {
		accessToken: null,
		refreshToken: null,
		clientId: 'a7d86f4d142a16084e26cb98b74f76f3',
		clientSecret: '11a75c57241f33c49724c2492533f76a',
		redirUrl: 'http://localhost:5000/close'
	},
	microsoft: {
		accessToken: null,
		refreshToken: null,
		clientId: '000000004C10C8FB',
		clientSecret: 'OrCZRGynNtc1Bz-KpbonAEunEwmPyXVh',
		redirUrl: 'http://bubbldomain.com:5000/microsoftauth' // Please ask me about this, I know it makes no sense
	}
};

// Configuration

sc.init(app.data.clientId, app.data.clientSecret, app.data.redirUrl);

app.configure(function () {
	app.set('views', __dirname + '/views');
	app.set('view engine', 'jade');
	app.use(express.bodyParser());
	app.use(express.methodOverride());
	app.use(express.logger());
	app.use(app.router);
	app.use(express.static(__dirname + '/public'));
	app.use(function (req,res) { // If a route is requested that doesn't exist, this will be the response
			res.send('404 - Not found');
			console.log('Requested page not found.');
	}); 
});

app.configure('development', function () {
	app.use(express.errorHandler({ dumpExceptions: true, showStack: true })); 
});

app.configure('production', function () {
	app.use(express.errorHandler());
});

// Routes - Misc.

app.get('/', function (req,res) {
	res.render("index");
});

app.get('/login/error', function (req,res) {
	res.send("error");
});

app.get('/checklogin', function (req,res) {
	status = [];
	for (var key in app.data) {
		if (app.data.hasOwnProperty(key)) {
			if (app.accessTokens[key] == null) {
				status.push({
					name: key.toString(),
					status: null
				});
			} else {
				status.push({
					name: key.toString(),
					status: 'ok'
				});
			}
		}
	}
	res.send(status);
});

// Instagram login

app.get('/instagramauth', function (req,pageRes) {
	if (req.query.origin == 'client') { // Determine who requested the page; if instagram is redirecting the client back to this endpoint, don't try to authenticate again
		if (app.data.instagram.accessToken == null) { // If we already have an access token, don't bother getting another
			pageRes.send('https://instagram.com/oauth/authorize/?client_id='+app.data.instagram.clientId+'&redirect_uri='+app.data.instagram.redirUrl+'&response_type=code');
		} else {
			pageRes.send('no');
		}
	} else if (req.query.code != null) {
		request.post( // If we got a response from the instagram auth endpoint, exchange our access code for a more permanent access token
			'https://api.instagram.com/oauth/access_token',
			{
				form: {
					client_id: app.data.instagram.clientId,
					client_secret: app.data.instagram.clientSecret,
					redirect_uri: app.data.instagram.redirUrl,
					grant_type: "authorization_code",
					code: req.query.code
				},
			},
			function (err,res,body) {
				console.log(body);
				body = JSON.parse(body);
				app.data.instagram.accessToken = body.access_token; // The User object could be retrieved here
				pageRes.redirect(app.data.authRedir) // This is where the client will be directed after they authenticate with instagram
			}
		);
	}
});

// Reddit Login

app.get('/redditauth', function (req,pageRes) {
	state = crypto.createHash('sha1');
	state.update("snootis");
	stateHash = state.digest('hex');
	if (req.query.origin == 'client') { // Determine who requested the page; if reddit is redirecting the client back to this endpoint, don't try to authenticate again
		if (app.data.reddit.accessToken == null) { // If we already have an access token, don't bother getting another
			pageRes.send('https://ssl.reddit.com/api/v1/authorize?state='+stateHash+'&response_type=code&scope=submit,mysubreddits&client_id='+app.data.reddit.clientId+'&redirect_uri='+app.data.reddit.redirUrl);
		} else {
			pageRes.send('no');
		}
	} else if (req.query.code != null) {
		request.post( // If we got a response from the reddit auth endpoint, exchange our access code for a more permanent access token
			'https://ssl.reddit.com/api/v1/access_token',
			{
				form: {
					state: stateHash, // A unique string for our app to make sure we made the request for the access token
					scope: "submit,mysubreddits", // Authorization scope. See reddit api. Also verify that these match in the long URL above
					client_id: app.data.reddit.clientId,
					redirect_uri: app.data.reddit.redirUrl,
					code: req.query.code,
					grant_type: "authorization_code"
				},
				auth: {
					user: app.data.reddit.clientId,
					pass: app.data.reddit.clientSecret,
					sendImmediately: false
				}
			},
			function (err,res,body) {
				body = JSON.parse(body);
				app.data.reddit.accessToken = body.access_token;
				pageRes.redirect(app.data.authRedir) // This is where the client will be directed after they authenticate with Reddit
			}
		);
	}
});

// Google Login

app.get('/googleauth', function (req,pageRes) {
	if (req.query.code != null) {
		request.post( // If we got a response from the google auth endpoint, exchange our access code for a more permanent access token
			'https://accounts.google.com/o/oauth2/token',
			{
				form: {
					code: req.query.code,
					client_id: app.data.google.clientId,
					client_secret: app.data.google.clientSecret,
					grant_type: "authorization_code",
					redirect_uri: app.data.google.redirUrl
				}
			},
			function (err,res,body) {
				if (err != null) {
					pageRes.send("error");
					return;
				}
				body = JSON.parse(body);
				app.data.google.accessToken = body.access_token;
				app.data.google.refreshToken = body.refresh_token;
				pageRes.send("ok");
			}
		);
	}
});

// Login to Soundcloud

app.post('/soundcloudauth', function (req,res) {
	console.log(req.body);
	app.data.soundcloud.accessToken = req.body.token;
	app.data.facebook.user = req.body.user;
	res.send('done');
});

// Microsoft login

app.get('/microsoftauth', function (req,pageRes) {
	console.log(req.query);
	if (req.query.code != null) {
		request.post( // If we got a response from the google auth endpoint, exchange our access code for a more permanent access token
			'https://login.live.com/oauth20_token.srf',
			{
				form: {
					code: req.query.code,
					client_id: app.data.microsoft.clientId,
					client_secret: app.data.microsoft.clientSecret,
					redirect_uri: app.data.microsoft.redirUrl,
					grant_type: "authorization_code"
				}
			},
			function (err,res,body) {
				body = JSON.parse(body);
				app.data.microsoft.accessToken = body.access_token;
				app.data.microsoft.refreshToken = body.refresh_token;
			}
		);
	}
});

//CODE TO GET FB COMMENTS - NOT USED FOR NOW, PLEASE IGNORE

/*if (res.data[i].comments != null) {
	for(j=0;j<res.data[i].comments.data.length;j++) {
		if (res.data[i].comments.data[j].from.id == app.data.facebook.user.id) {
			posts.push(res.data[i].comments.data[j].message);
		}
	}
}*/

// Routes - Facebook Login

app.post('/fbauth', function (req,res) {
	app.data.facebook.accessToken = req.body.token; // The client authenticates itself with the FB JS SDK, so all we do here is collect the client's access token and user id
	app.data.facebook.user = req.body.user;
	res.send("done");
});

// Routes - Metafeed

// Get Posts

app.get('/posts', function (req,pageRes) {
	posts = [];
	openRequests = 1; // Keep track of how many requests we've made to content providers
	callback = function () { // Set up a method that is called when each contnet provider's request completes
		openRequests--;
		if (openRequests == 0) { // If we're the last one, we can send the collected data (posts) to the client
			shuffle(posts); // Just shuffle the array of posts to make things look interesting, see helper method below
			pageRes.send(posts);
		}
	}
	getFacebook(callback,pageRes); // Request posts from FB and Reddit and Instagram
	getReddit(callback,pageRes);
	//getInstagram(callback,pageRes);
});

// Get posts from Instagram

getInstagram = function (callback,pageRes) {
	request.get( // Figure out what the person's default subreddits are
		{
			url: 'https://api.instagram.com/v1/users/self/feed?access_token='+app.data.instagram.accessToken,
		},
		function (err,res,body) {
			body = JSON.parse(body);
			if (body.data != null) {
				for (i=0;i<body.data.length;i++) {
					post = body.data[i];
					tags = "";
					if (tags != null) {
						for (j=0;j<post.tags.length;j++) {
							tags += post.tags[j];
						}
					}
					formattedPost = postObject('instagram',post.user.username,post.user.profile_picture,post.likes.count,post.created_time,post.images.standard_resolution.url,'image',tags);
					posts.push(formattedPost);
				}
				callback();
			} else {
				pageRes.send("error");
				console.log("Login error from Instagram");
			}
		}
	);
}

// Get posts from FB

getFacebook = function (callback,pageRes) {
	Facebook.api('/'+app.data.facebook.user+'/home', 'get', { access_token: app.data.facebook.accessToken }, function (res) { // Grab the first 20? posts from our news feed
		if (res.data != null) {
			postCount = res.data.length; // We have to do a miniature version of what we did above to keep track of individual open requests above here with each request for the person's profile pic
			parsePost = function (i) {
				Facebook.api('/'+res.data[i].from.id+'/picture?type=small&redirect=false', 'get', function (imgRes) { // Get the link to each person's profile pic; can be changed to get a larger profile picture
					post = res.data[i]; // Convert the raw response from FB into one of our nice little 'post' objects depending on content
					if (post.likes != null) {
						likes = post.likes.data.length;
					} else {
						likes = 0;
					}
					if (post.picture != null) {
						formattedPost = postObject('facebook',post.from.name,imgRes.data.url,likes,post.updated_time,post.picture,'image',post.message || post.story);//TODO: Decide on universal date format. Also pictures should probably be hyperlinked
					} else if (post.message != null) {
						formattedPost = postObject('facebook',post.from.name,imgRes.data.url,likes,post.updated_time,post.message || post.story,'text',(post.message || post.story).slice(0,80)+"...");//TODO: Decide on universal date format. Also pictures should probably be hyperlinked
					} else {
						formattedPost.message =  "******* NOT A MESSAGE OR STORY OR PIC *******";
					}
					postCount--;
					posts.push(formattedPost); // Add post to posts array
					if (postCount == 0) { // If we're the last one, run the callback from above
						callback();
					}
				});
			}
			for(i=0;i<res.data.length;i++) { // DO the above for every FB post we got back
				parsePost(i);
			}
		} else {
			pageRes.send("error");
			console.log("login error from FB"); // If we weren't logged in, say so
		}
	});
}

// Get posts from Reddit

getReddit = function (callback,pageRes) {
	request.get( // Figure out what the person's default subreddits are
		{
			url: 'https://oauth.reddit.com/reddits/mine/subscriber?limit=100',
			headers: {
				Authorization: 'bearer '+app.data.reddit.accessToken
			},
			auth: {
				user: app.data.reddit.clientId,
				pass: app.data.reddit.clientSecret,
				sendImmediately: false
			}
		},
		function (err,res,body) {
			if (JSON.parse(body).data != null) {// TODO: Fix request failing if user has no subscribed subreddits
				subreddits = JSON.parse(body).data.children;
				subscriptions = subreddits[0].data.display_name;
				for (i=1;i<subreddits.length;i++) { // For each subreddit, add it's name to a big multireddit stored in 'subscriptions'
					subscriptions+="+"+subreddits[i].data.display_name;
				}
				request.get( // Grab the first 30 posts from this multireddit
					{
						url: 'http://reddit.com/r/'+subscriptions+'.json?limit=30'
					},
					function (err,res,body) { // Convert the raw response from Reddit into one of our nice little 'post' objects depending on content
						rawPosts = JSON.parse(body).data.children;
						parsePost = function (i) {
							post = rawPosts[i].data;
							if (post.selftext != null) {
								formattedPost = postObject('reddit',post.author,null,post.score,post.created_utc,post.selftext,'text',post.title); //TODO: Decide on universal date format
							} else {
								if (post.url.indexOf('imgur') != -1) {
									formattedPost = postObject('reddit',post.author,null,post.score,post.created_utc,"http://i."+post.url.slice(7,post.url.length)+".png",'image',post.title); //TODO: Decide on universal date format
								} else {
									formattedPost = postObject('reddit',post.author,null,post.score,post.created_utc,post.url,'link',post.title); //TODO: Decide on universal date format
								}
								
							}
							posts.push(formattedPost); // Add the new post to our list
						};
						for (i=0;i<rawPosts.length;i++) { // Do the above for every post in our array of posts we got back from reddit
							parsePost(i);
						}
						callback(); // Run the callback to see if the Reddit request was the last one to complete
					}
				);
			} else {
				pageRes.send("error");
				console.log("login error from Reddit"); // If we weren't logged in, say so
			}
		}
	);
}

// Post to other services

app.post('/post', function (req,pageRes) {
	console.log(req.body);
	postToReddit(req.body);
	postToFB(req.body);
})

// Post posts to Reddit

postToReddit = function (post) {
	if (post.content.kind != 'text') { // If we're posting with 'image' or 'link' as the type fo the content, make a link post
		console.log("posting link");
		post = {
			api_type: 'json',
			kind: 'link',
			sr: 'test',
			url: post.content.data,
			title: post.title
		}
	} else {
		post = { // If we're posting with 'text' as the type of the content, make a self post
			api_type: 'json',
			kind: 'self',
			sr: 'test',
			text: post.content.data,
			title: post.title
		}
	}
	request.post( // Simply send off the post to Reddit
		{
			url: 'https://oauth.reddit.com/api/submit',
			headers: {
				Authorization: 'bearer '+app.data.reddit.accessToken
			},
			form: post,
			auth: {
				user: app.data.reddit.clientId,
				pass: app.data.reddit.clientSecret,
				sendImmediately: false
			}
		},
		function (err,res,body) { // It's as easy as that
			body = JSON.parse(body);
			console.log(err);
			console.log(body);
			console.log(body.json.errors);
			if (body.json.captcha != null) { // CAPTCHAs REEEEEALLY suck. We can't deal with them right now
				console.log("Had captcha, nothing we can do at this point. Complain to Stephen. Sorry.");
			}
		}
	);
}

// Post posts to FB

postToFB = function (post) {
	if (post.content.kind != 'text') {
		body = {
			access_token: app.data.facebook.accessToken,
			name: post.title,
			link: post.content.data,
			message: post.title
		}
	} else {
		body = {
			access_token: app.data.facebook.accessToken,
			message: post.content.data
		}
	}
	Facebook.api('/me/feed', 'post', body, function (res) {
			console.log(res);
		} // Obviously doesn't work
	);
}

// Post posts to Instagram
// Impossible with current Instagram API

// Routes - Music

app.get('/music/search', function (req,pageRes) {
	openReqs = 2;
	results = [];
	callback = function () {
		if (--openReqs == 0) {
			shuffle(results);
			pageRes.send(results);
		}
	}
	request.get('https://api.soundcloud.com/tracks.json?client_id='+app.data.soundcloud.clientId+'&q='+req.query.q+"&limit=30", function (err,res,body) {
		data = JSON.parse(body);
		console.log(data);
		for (i=0;i<data.length;i++) {
			song = data[i];
			formattedSong = songObject('soundcloud',song.title,song.user.username,null,song.genre,song.artwork_url,song.favoritings_count,song.release_year,song.id);
			results.push(formattedSong);
		}
		callback();
	});
	request.get('https://gdata.youtube.com/feeds/api/videos?q='+req.query.q+'&max-results=30&v=2&alt=jsonc', function (err,res,body) {
		data = JSON.parse(body).data.items;
		for (i=0;i<data.length;i++) {
			song = data[i];
			formattedSong = songObject('youtube',song.title,null,null,null,null,song.likeCount,song.uploaded,song.id);
			results.push(formattedSong);
		}
		callback();
	});
});

// Routes - Messaging

// Send an email with Gmail

app.post('/email', function (req,res) {
	if (req.body.service == "gmail") {
		if (app.data.google.accessToken == null) { // If we aren't already logged into google, login to google with this monster below
			loginToService('gmail');
		} else {
			sendGmail(req.body.message);
		}
	} else if (req.body.service == "outlook" || req.body.service == "live") {
		sendOutlookMail(req.body.message);
	}
	console.log(req.body);
});

// Functions to send mail with verious services

// Send mail with outlook/live

sendOutlookMail = function (message) {
	email = "stksullivan@live.com";
	name = "Stephen Sullivan"
	transport = nodemailer.createTransport("SMTP", { // Use the nodemailer library to send mail
		service: "hotmail",
		auth: {
			user: email,
			pass: "3l3cTr0.Z@p:"
		}
	});
	mailOptions = {
		from: name+"<"+email+">",
		to: message.to,
		subject: message.subject,
		text: message.body,
		html: message.htmlBody
	}
	console.log(message)
	transport.sendMail(mailOptions, function (error,response) {
		if (error) {
			console.log("error was");
			console.log(error);
		} else {
			console.log("Message sent: " + response.message);
		}

		// if you don't want to use this transport object anymore, uncomment following line
		transport.close(); // shut down the connection pool, no more messages
	});
}

// Send mail with Gmail

sendGmail = function (message) {
	email = "stksullivan@gmail.com";
	name = "Stephen Sullivan"
	transport = nodemailer.createTransport("SMTP", { // Use the nodemailer library to send mail
		service: "Gmail",
		auth: {
			XOAuth2: { // Gmail's version of OAuth2
				user: email, // We will get this from the DB eventually
				clientId: app.data.google.clientId,
				clientSecret: app.data.google.clientSecret,
				refreshToken: app.data.google.refreshToken,
				accessToken: app.data.google.accessToken,
				timeout: 3600 // I do not know the function of this timeout
			}
		}
	});
	mailOptions = { // Used to define the email we will send
		from: name+"<"+email+">", // Again, both of these will be retrieved from the DB eventually
		to: message.to, // list of receivers
		subject: message.subject, // Subject line
		text: message.body, // plaintext body
		html: message.htmlBody // html body
	}

	// send mail with defined transport object
	transport.sendMail(mailOptions, function (error,response) {
		if (error) {
			console.log("error was");
			console.log(error);
		} else {
			console.log("Message sent: " + response.message);
		}

		// if you don't want to use this transport object anymore, uncomment following line
		transport.close(); // shut down the connection pool, no more messages
	});

	/* FOR REFERENCE, GOOGLE API PROCESS
	OAuth2 = googleapis.OAuth2Client; // Set up our OAuth2 reference thing
	oauth2Client = new OAuth2(app.data.google.clientId, app.data.google.clientSecret, app.data.google.redirUrl);
	oauth2Client.credentials = {
		access_token: app.data.google.accessToken,
		refresh_token: app.data.google.refreshToken
	};
	googleapis.discover('plus','v1').execute(function (err,client) { // Load the Google+ API
		console.log("Loaded Google+ library.");
		req = client.plus.people.get({ userId: 'me' }).withAuthClient(oauth2Client); // Set up a request to get your info from G+
		req.execute(function (err,res) {
			console.log("g+ debug");
			console.log(err);
			console.log(res);
			//sendMail("Stephen Sullivan","stksullivan@gmail.com");
			sendMail(res.displayName,res.emails[0].value); // Send the email with YOUR email and YOUR name
			console.log("done"); // This line is necessary for some reason, otherwise the above line is executed twice. Yes, you read
								 // that right, the line above reliably executes twice unless you include this console.log().
		});
	});*/
}

app.post('/email', function (req,pageRes) {
	imap = new Imap({
		user: 'stksullivan@live.com',
		password: '3l3cTr0.Z@p:',
		host: 'imap-mail.outlook.com',
		port: 993,
		tls: true,
		tlsOptions: { rejectUnauthorized: false }
	});

	imap.once('ready', function () {
		imap.openBox('INBOX', true, function (err,box) {
			if (err) {
				console.log("openBox error: "+err);
			} else {
				f = imap.seq.fetch(box.messages.total-2+":"+box.messages.total, {
					bodies: ['HEADER.FIELDS (FROM TO SUBJECT)','TEXT']
				});
				messages = [];
				f.on('message', function (msg,seqno) {
					message = {
						from: null,
						to: null,
						subject: null,
						body: null
					}
					msg.on('body', function (stream,info) {
						if (info.which == 'TEXT') {
							messageBuffer = "";
							stream.on('data', function (chunk) {
								messageBuffer += chunk.toString('utf8');
							});
							stream.once('end', function () {
								message.body = messageBuffer;
							});
						} else {
							buffer = "";
							stream.on('data', function (chunk) {
								buffer += chunk.toString('utf8');
							});
							stream.once('end', function () {
								headers = Imap.parseHeader(buffer);
								message.subject = headers.subject[0];
								message.from = headers.from[0];
								message.to = headers.to[0];
							});
						}
					});
					msg.once('end', function () {
						messages.push(message);
					});
				});
				f.once('error', function (err) {
					console.log('Fetch error: ' + err);
				});
				f.once('end', function () {
					pageRes.send(messages);
					imap.end();
				});
			}
		});
	});

	imap.once('error', function(err) {
		console.log("imap error: "+err);
	});

	imap.once('end', function() {
		console.log('Connection ended');
	});

	imap.connect();
});

// Routes - Actual Pages

/*app.get('/email', function (req,pageRes) { // Render the /test page
	pageRes.render("email");
});*/

app.get('/testemail', function (req,pageRes) { // Render the /test page
	pageRes.render("testEmail");
});

app.get('/fbmessage', function (req,pageRes) { // Render the /test page
	pageRes.render("fbmessage");
});

app.get('/close', function (req,res) {
	res.render('close');
});

app.get('/bubbllogin', function (req,pageRes) { // Render the /test page
	pageRes.render("bubbllogin");
});

// Routes for iFrame tests

app.get('/feedF', function (req,pageRes) { // Render the feed content
	pageRes.render("feedF");
});

app.get('/playerF', function (req,pageRes) { // Render player (footer) content
	pageRes.render("playerF");
});

app.get('/musicF', function (req,pageRes) { // Render music content
	pageRes.render("musicF");
});

// Helper Functions

function loginToService (service,res) {
	if (service == "microsoft") {
		res.send("Please login to Microsoft.<br><a href='https://login.live.com/oauth20_authorize.srf?client_id="+app.data.microsoft.clientId+"&scope=wl.basic wl.offline_access&response_type=code&redirect_uri="+app.data.microsoft.redirUrl+"'>Here</a>");
	} else if (service == "google") {
		res.send("Please login to Google.<br><a href='https://accounts.google.com/o/oauth2/auth?scope=profile email https://mail.google.com/&redirect_uri="+app.data.google.redirUrl+"&response_type=code&client_id="+app.data.google.clientId+"&access_type=offline'>Here</a>");
	}
}

function postObject (src,from,fromImage,score,age,content,kind,title) { //TODO: Decide on universal post specification
	return { // This method simply provides an easy way to make a post object in one line
		'source': src,
		'from': {
			'name': from,
			'image': fromImage
		},
		'score': score,
		'age': age,
		'content': {
			'kind': kind,
			'data': content
		},
		'title': title
	}
}

function songObject (src,title,artist,album,genre,artwork,score,age,id) { //TODO: Decide on universal song specification
	return { // This method simply provides an easy way to make a song object in one line
		'source': src,
		'metadata': {
			'title': title,
			'artist': artist,
			'album': album,
			'genre': genre,
			'artwork': artwork
		},
		'score': score,
		'age': age,
		'id': id,
	}
}

function shuffle (array) { // I did not write this code, this was shamelessly ganked off of Stackoverflow
	var currentIndex = array.length,temporaryValue,randomIndex;

	// While there remain elements to shuffle...
	while (0 !== currentIndex) {

		// Pick a remaining element...
		randomIndex = Math.floor(Math.random() * currentIndex);
		currentIndex -= 1;

		// And swap it with the current element.
		temporaryValue = array[currentIndex];
		array[currentIndex] = array[randomIndex];
		array[randomIndex] = temporaryValue;
	}

	return array;
}

// Routes - DB Operations

// Create a bubbl account in Database

app.get('/createaccount', function(req) {
	
	db.open(function(err, db) { // We're going to start working on the Database
		var coll = db.collection('candy'); // Our collection of documents (our database, or DB) is under the name candy, so lets get that collection

		coll.findOne({ user:req.query.username, password:req.query.password }, function (err,doc) { // We want to FIND ONE document in the collection
																							  // where the user field is the specified username
																							  // and the password is the specified password.
																							  // We get these specified values from the query
																							  // of the GET request.
			if (doc == null) { // If there is NOT already such a document that satifsfies the above...
				coll.insert([{user:req.query.username, password:req.query.password, address:req.connection.remoteAddress}], function(err, result){ // Then instert the new user's info into the DB
				});
				console.log("User record added.");
				db.close(); // Stop working on the DB
			} else { // If we did find a document that satisfies the above...
				console.log("User already exists!"); // Then the user has always been registered
				db.close(); // Stop working on the DB
			}
		});

	});
});

// Check to see if an account exists in the database
app.get('/logmein', function(req) {
	db.open(function(err, db) { // Start working on our DB
		var coll = db.collection('candy'); // Get our specific Collection, candy
		coll.findOne({ user:req.query.username, password:req.query.password }, function (err,doc) { // Again, FIND ONE document in our collection
																									// where username is the username, and password
																									// is the password.
			if (doc == null) { // If we don't find a matching document, the user does not exist.
				console.log("User does not exist!");
				db.close(); // Stope working on the DB.
			} else { // If we DO find a matching document...
				console.log("User found, saving IP address");
				coll.findAndModify( // If we DO find a document, then find that document again (I know, it's a bit redundant) and modify it
					{ user:req.query.username, password:req.query.password }, // Criteria to FIND the document
					[['user', 1]],
					{ user:req.query.username, password:req.query.password, address:req.connection.remoteAddress }, // Replace the document with this information
					function (err, doc2) { // This method is called once the document is found.
						if (err == null) {
							console.log("IP address saved.");
						} else {
							console.log(err);
							console.log("Error saving IP address!");
						}
						db.close(); // As always, stop working on the DB
					}
				);
			}
		});
	});
});
//create playlist in DB
app.get('/createplaylist', function(req) {
	db.open(function(err, db) { // Start working on our DB
		var coll = db.collection('playlists'); //Get or create specific collection
		coll.findOne({ name:req.query.playlistname }, function (err,doc) {
			if(doc == null) {//playlist does not exist
				console.log('creating new playlist');
				coll.insert([{ name: req.query.playlistname, songs: req.query.playlistsongs}], function(err, result){ // Then instert the new user's info into the DB
				});
			} else{ //playlist does exist
				console.log('Playlist already exists!');
			}
			db.close(); //close it up bitches
		});
	});
});
//change things in individual playlists
app.get('/showplaylist', function(req,res) {
	db.open(function(err,db) {//open up the db
		var coll = db.collection('playlists');
		coll.findOne({name:req.query.playlistname}, function(err,doc){
			if(doc==null){ //if the playlist doe not exist
				console.log('This playlist does not exist');
				db.close();
			} else{
				console.log('Playlist exists');
				console.log(doc);
				var passedArray = doc.songs;
				res.send(passedArray);
				db.close();
			}
		});
	});
});

app.listen(process.env.PORT || 5000); // Start our app on port 5000, or if it's running on heroku, what ever port they specify for us