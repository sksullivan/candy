import java.io.InputStream;
import java.io.IOException;
import java.net.URL;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.StringTokenizer;
import javax.json.stream.*;
import javax.json.*;
import java.util.*;
/* Write a description of class fdsfdsf here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */

public class Reader
{
    public ArrayList<Post> readJSON()
    {
        URL url = null;
        InputStream is = null;
        try
        {
            url = new URL("http://localhost:5000/posts");
        }
        catch(MalformedURLException e)
        {}
        try
        {
            is= url.openStream();
        }
        catch(IOException e)
        {}
        JsonParser parse = Json.createParser(is);
        JsonParser.Event e;
        String source = "null";
        String name = "null";
        String image = "null";
        String score = "null";
        String age = "null";
        String kind = "null";
        String data = "null";
        String title = "null";
        ArrayList<Post> postlist = new ArrayList<Post>();
        while(parse.hasNext())//interates through all the "tokens" in the JSON array
        {
            e = parse.next();//advances to next token
            if(e == JsonParser.Event.KEY_NAME)//checks if the current token is actual text field
            {   String temp = parse.getString();//temp string will have the field name
                e = parse.next();//advances to content field
                if(e != JsonParser.Event.VALUE_NULL)
                {
                    switch(temp)//parse JSON content to variables
                    {
                        case "source": 
                        source = parse.getString();
                        break;
                        case "name": 
                        name = parse.getString();
                        break;
                        case "image": 
                        image = parse.getString();
                        break;
                        case "score": 
                        score = parse.getString();
                        break;
                        case "age": 
                        age = parse.getString();
                        break;
                        case "kind": 
                        kind = parse.getString();
                        break;
                        case "data": 
                        data = parse.getString();
                        break;
                        case "title": //title is last field, so a Post object can be created with the variables that have been save previously and the cycle begins again
                        title = parse.getString();
                        postlist.add(new Post(source, name, image, score, age, kind, data));
                        source = "null";
                        name = "null";
                        image = "null";
                        score = "null";
                        age = "null";
                        kind = "null";
                        data = "null";
                        title = "null";
                        break;
                    } 
                }
            }
        }
        parse.close();
        return postlist;//return arraylist with all of the posts in objects
    }
    //parses data from post object into indivual words and puts them in an array of strings. only takes words from facebook text posts
    public ArrayList<String> parseList(ArrayList<Post> postlist)
    {   ArrayList<String> feed = new ArrayList<String>();
        for(int x=0; x<postlist.size();x++)
        {   
            if(postlist.get(x).getSource().equals("facebook")&&postlist.get(x).getKind().equals("text"))
            {
                String[] temp = new String[postlist.get(x).getData().split("\\W").length];//creates array the exact length that is needed
                temp = postlist.get(x).getData().split("\\W"); //String.split separates each word and puts in in array
                for (int y=0;y<temp.length;y++)
                {
                    feed.add(temp[y].toUpperCase());//adds to arraylist in uppercase
                }
            }
        }

        return feed;
    }

    public ArrayList<Word> countWord(ArrayList<String> feed)
    { //returns arraylist with all the words from your posts in their own objects and counted
        ArrayList<Word> countWords = new ArrayList<Word>();

        //declaring arraylist and doing the first object manually so the for loop dont break
        countWords.add(new Word(feed.get(0)));
        for(int x=1;x<feed.size();x++)
        //x for loop iterates through the raw list of words parsed from the JSON object
        {
            boolean flag = false;
            //flag to designate of the word from the feed arraylist was already found in the countwords arralist
            for(int y=0;y<countWords.size();y++)
            {//y for loop iterates thorugh countWords to check if the word from feed should be added or counted
                if(countWords.get(y).getWord().equals(feed.get(x)))
                {// checks if the word from feed is already in countWords, if it is it breaks the y loop
                    countWords.get(y).incQuantity();
                    flag = true;
                    break;
                }
            }
            if(!flag)
            {//checks if the word from feed was already counted, if not we add it
                countWords.add(new Word(feed.get(x)));
            }
        }
        return countWords;
    }

    public ArrayList<Word> sortWordListAlpha(ArrayList<Word> w)
    {
        //returns arraylist sorted alphabetically A-Z
        Collections.sort(w, new CompareAlpha());
        return w;
    }

    public ArrayList<Word> sortWordListNum(ArrayList<Word> w)
    {
        //returns arraylist sorted by quantity greatest to least
        Collections.sort(w, new CompareNum());
        return w;
    }

    public ArrayList<Word> lettersOnly(ArrayList<Word> w)
    {
        //returns arraylist with objects that contain regex non letters removed and cleans up short words
        
        for(int x = w.size()-1;x>=0;x--)
        {
            if(w.get(x).getWord().matches("\\d")||w.get(x).getWord().matches("\\W")||w.get(x).getWord().equals("")||w.get(x).getWord().equals(" "))
               {
                   w.remove(x);
                   
                }
                if(w.get(x).getWord().length() < 4)
                {
                    System.out.println("removed " + w.get(x).getWord());
                    w.remove(x);
                    
            }
        }
      
        return w;
    }
}
