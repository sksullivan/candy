import com.mongodb.BasicDBObject;
/**
 * Write a description of class Word here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class Word extends BasicDBObject
{
    //Object for storing a word and its quantity, it always starts a quantity of 1
    String word;
    int quantity = 1;
    public Word(String word)
    {
        super();
        this.word = word;
    }
    public String getWord()
    {
        return word;
    }
    public int getQuantity()
    {
        return quantity;
    }
    public void incQuantity()
    {
        quantity++;
    }
    public int getLength()
    {
        //so I don't have to type like 7 extra letters
        return word.length();
    }
    public void setQuantity(int num)
    {
        quantity = num;
    }
    //This next method is eventually going to search a dictionary and find what category it belongs in
    //For now its just going to search a list of words for the word
    public void getCategory()
    {
        
    }
    public void DBObject()
    {
        this.put("word", word);
        this.put("quantity", quantity);
    }
    }

