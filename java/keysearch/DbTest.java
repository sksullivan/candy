import com.mongodb.MongoClient;
import com.mongodb.MongoException;
import com.mongodb.WriteConcern;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBObject;
import com.mongodb.BasicDBObject;
import com.mongodb.DBCursor;
import com.mongodb.ServerAddress;
import java.util.ArrayList;
/**
 * Write a description of class DbTest here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class DbTest
{MongoClient mongoClient;
    DB db;
    public DbTest()
    {
        try
        {
            mongoClient = new MongoClient( "localhost" , 27017 );
        }
        catch(java.net.UnknownHostException e)
        {}
        db = mongoClient.getDB( "candy" );
    }
    //writes the database of word objects and updates the quantity if it already exists
    public void writeDb(ArrayList<Word> boner)
    {
        DBCollection coll = db.getCollection("candy");
        
        for(int x=0;x<boner.size();x++)
        {   boner.get(x).DBObject();
            if(coll.findOne(boner.get(x))==null)
            {
                DBObject keyword = new BasicDBObject();
                DBObject user = new BasicDBObject();
                keyword.put("keyword",boner.get(x));
                user.put("user", keyword);
                coll.insert(user);
            }
            else
            {
                Word tempword = new Word(boner.get(x).getWord());
                tempword.setQuantity(boner.get(x).getQuantity());
                tempword.DBObject();
                coll.findAndModify(boner.get(x), tempword); 
            }

        }
       
    }

    //prints database of words
    public void readDb()
    {
        DBCollection coll = db.getCollection("candy");
        DBCursor cursor = coll.find().sort( new BasicDBObject( "quantity" , -1 ) );

        try {
            while(cursor.hasNext()) {
                System.out.println(cursor.next());
            }
        } finally {
            cursor.close();
        }
    }
}
