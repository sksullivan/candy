import javax.json.*;
import java.util.ArrayList;


public class WebAccess {
    ArrayList<Word> boner;
 
 
    /** Creates a new instance of HelloWorld */
    public WebAccess(ArrayList<Word> boner) {
        this.boner = boner;
    }
 
   
    public JsonArray getJson() {
        return buildJson();
    }
   //this method creates a JSON array of Word objects
    public JsonArray buildJson()
    {
       JsonArrayBuilder wordarray = Json.createArrayBuilder();
       JsonArray word = null;
       for(int x=0;x<boner.size();x++)
       {
           wordarray.add(wordJson(x,boner));
       }
       word = wordarray.build();
        return word;
    }
    //this method builds and returns a single JSON object of a Word object
    public JsonObjectBuilder wordJson(int x, ArrayList<Word> boner)
    {
        JsonObjectBuilder obj = Json.createObjectBuilder()
                .add("word",boner.get(x).getWord())
                .add("quantity",boner.get(x).getQuantity());
                
                return obj;
    }
}