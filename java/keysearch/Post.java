
/**
 * Write a description of class shit here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class Post
{
    String source = "null";
    String name = "null";
    String image = "null";
    String score = "null";
    String age = "null";
    String kind = "null";
    String data = "null";
    public Post(String source, String name, String image, String score, String age, String kind, String data)
    {
        this.source = source;
        this.name = name;
        this.image = image;
        this.score = score;
        this.age = age;
        this.kind = kind;
        this.data = data;
    }
    public String getSource()
    {
        return source;
    }
    public String getName()
    {
        return name;
    }
    public String getImage()
    {
        return image;
    }
    public String getScore()
    {
        return score;
    }
    public String getAge()
    {
        return age;
    }
    public String getKind()
    {
        return kind;
    }
    public String getData()
    {
        return data;
    }
}
