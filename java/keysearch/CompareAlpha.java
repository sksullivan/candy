import java.util.*;
/**
 * Write a description of class Compare here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class CompareAlpha implements Comparator<Word>
{
    public int compare(Word x, Word y)
     {
          //Custom compare method for Comparator so you can sort alphabetically
         return x.getWord().compareTo(y.getWord());
     }
}
