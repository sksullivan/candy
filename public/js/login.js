// FACEBOOK - Load SDK

$(document).ready(function () {
	$.ajaxSetup({ cache: true });
	$.getScript('//connect.facebook.net/en_UK/all.js', function () {
		FB.init({
			appId: '166064833600885',
		});
	});
	$.getScript('//connect.soundcloud.com/sdk.js', function () {
		SC.initialize({
			client_id: 'a7d86f4d142a16084e26cb98b74f76f3',
			redirect_uri: 'http://localhost:5000/close'
		});
	});
	googleSetup();
});

// Login to FB

function loginToFB () {
	// Code to send credentials to backend automatically when we're logged in to FB
	FB.getLoginStatus(function (response) {
		if (response.status === 'connected') {
			id = response.authResponse.userID;
			access_token = response.authResponse.accessToken;
			sendFBCredentialsToServer(access_token,id, function () {
				alert("Already logged into FB.");
			});
			return;
		}
		access_token = "";
		FB.login(function (response) {
			if (response.authResponse) {
				console.log(FB.getAuthResponse());
				access_token = FB.getAuthResponse()['accessToken'];
				id = FB.getAuthResponse()['userID'];
				sendFBCredentialsToServer(access_token, id, function () {
					alert("Logged in to FB!");
				});
			} else {
				console.log('FB LOGIN ERROR: User cancelled login or did not fully authorize.');
			}
		}, { scope: 'publish_actions,publish_stream' }); // The specific permissions we need from FB
	});

}

// Send login info to the backend

function sendFBCredentialsToServer (atoken,id,callback) {
	$.post("/fbauth", { token: atoken, user: id }, function () {
		callback();
	});
}

// GOOGLE - Load up the Google JS SDK - Not used for now

function googleSetup () {
	setLoginButton = function () {
		gapi.signin.render("subsearch", { // This makes the DOM element with id = "subsearch" become the login button for google
			'callback': googleCallback,
			'clientid': '208173405027-qptm09scplgho099tqorj2fmg41b75lt.apps.googleusercontent.com',
			'cookiepolicy': 'single_host_origin',
			//'requestvisibleactions': 'http://schemas.google.com/AddActivity',
			'scope': 'https://www.googleapis.com/auth/youtube.readonly'
		});
	}

	var po = document.createElement('script');
	po.type = 'text/javascript';
	po.async = false;
	po.src = 'https://plus.google.com/js/client:plusone.js?onload=setLoginButton';
	var s = document.getElementsByTagName('script')[0];
	s.parentNode.insertBefore(po, s);
}

// What to do when we get a response back from Google

function googleCallback (googleAuthResult) {
	if (googleAuthResult['code']) {// If we got a good response back
		$.post('/googleauth', { code: googleAuthResult['code'] }, function (data) { // Send that code to the server so the server can exchange it for an access token
			if (data != "error") {
				alert("Logged into Google!");
			} else {
				alert("Server error logging into Google.");
			}
		});
	} else {
		console.log("GOOGLE LOGIN ERROR: "+googleAuthResult['error']);
	}
}

// REDDIT - Login to Reddit

function loginToReddit () {
	$.get('/redditauth', { origin: 'client' }, function (res) {
		if (res == "no") {
			alert("Already logged in to Reddit.");
		} else {
			window.open(res);
		}
	});
}

// INSTAGRAM - Login to Instagram

function loginToInstagram () {
	$.get('/instagramauth', { origin: 'client' }, function (res) {
		if (res == "no") {
			alert("Already logged in to Instagram.");
		} else {
			window.open(res);
		}
	});
}

// SOUNDCLOUD - Login to Soudcloud

function loginToSoundcloud () {
	SC.connect(function() {
		SC.get('/me', function(me) {
			var keys = Object.keys(SC);
			theToken = SC.accessToken();
			alert('Hello, ' + me.username);
			$.post('/soundcloudauth', { token: theToken, user: me }, function (data) {
				alert("Logged into SoundCloud!");
			});
		});
	});
}

// EVERYTHING - Login combined

function loginToEverything() {
	// Login to Google already happens as soon as you click the designated login button in setLoginButton
	loginToFB();
	//loginToInstagram();
	loginToReddit();
	//loginToSoundcloud();
}

// Check login status

function checkLoginStatuses() {
	$.get('/checklogin', function (res) {
		for (i=0;i<res.length;i++) {
			status = res[i].name+": "+res[i].status;
			console.log(status);
			$('#status').append(
				$('<div/>', {
					text: status
				})
			);
		}
	});
}