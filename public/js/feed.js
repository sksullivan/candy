//$(document).ready(function () {getPostsFromBackend();});

function getPostsFromBackend () {
	$.get("/posts", function (data) { // GET posts from server
		console.log(data);
		if (data != "error") { // If we got good data...
			for (i=0;i<data.length;i++) { // For every post in the response...
				
				// At this point you can do whatever jQuery or JS or CSS you want with the above data
				// For example, like this:

				$('#metafeed').append(
					$('<div/>', {
						id: 'post'+i,
						class: 'post '+data[i].source // Set the CSS class to be the source of the post, i.e. 'reddit', 'facebook', etc
					}).data(data[i]) // This clever little line stores the object retured from the server in the actual DOM Element we're making, so we can retrieve it if we want to repost this post
				);
				console.log("appended to metafeed");
				
				//These next two append sections add two main spaces to the post div
				$('#post'+i).append(
					$('<div/>', {
					id: 'imagespace'+i,
					class: 'imagespace'
					})
				);
				$('#post'+i).append(
					$('<div/>', {
					id: 'textspace'+i,
					class: 'textspace'
					})
				);
				$('#textspace'+i).append($('<div/>', {
					text: data[i].from.name,
					class: 'postfrom'
				}));
				if(data[i].source == 'reddit')
				{
					$('#imagespace'+i).append($('<img>', {
					src: '/assets/reddit.png',
					class: 'userimage'
				}));
				} else {
					$('#imagespace'+i).append($('<img>', {
					src: data[i].from.image,
					class: 'userimage'
				}));
				}
				if (data[i].content.kind == 'image') {
					//adds div to post to hold images in posts (not to be confused with profile pictures)
					$('#post'+i).append($('<div/>', {
						id: 'postimagespace'+i,
						class: 'postimagespace'
						}));
						
					$('#postimagespace'+i).append($('<img>', {
						src: data[i].content.data,
						class: 'postimage'
					}));
					$('#textspace'+i).append($('<div/>', {
						text: data[i].title,
						class: 'posttitle'
				}));
				} else 
				{
					if(data[i].content.data.length > 80)
					{
						$('#textspace'+i).append($('<div/>', {
							text: data[i].title,
							class: 'posttext'
						}));
						$('#post'+i).append($('<div/>', {
							id: 'postdropdown',
							class: 'postdropdown',
							text: 'Read More'
						}));
					} else
					{
						$('#textspace'+i).append($('<div/>', {
							text: data[i].content.data,
							class: 'posttext'
						}));
					}
				}
				/*$('#post'+i).append($('<div/>', {
					onclick: 'sendPostToBackend('+i+')', // onclick method calls the below method and pulls data stored in the DOM Element, notice that this div's number is passed in as a parameter
					text: 'SHARE'
				}));*/
				// ... Do as you please
			}
		} else {
			alert("There was some sort of login error "); // If we got a bad response, we probably didn't log in
		}
	});
}
function sendPostToBackend (id) { // id specifies which DOM Element to pull data from
	data = $('#post'+id).data(); // Grab a local reference to the data stored in that DOM Element
	$.post("/post", data, function (data) { // Send this data to the server at /post
		alert("Check the server output to make sure things went ok.");
	});
}