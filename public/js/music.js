playlistArray = new Array(); // Set up song queue array
playlistPosition = 0;
playlistPlayer = null;// Set up a global reference to the Playlist song that is playing
playlistState = null;

ytplayer = null; // Set up a global reference to the Youtube player
ytState = null; // Used to keep track of the status of the player EX: paused playing null etc

scplayer = null; // Set up a global reference to the Souncloud song that is playing
scState = null;

window.onYouTubePlayerReady = function (playerId) { // Called when the player is finished loading
	ytplayer = document.getElementById(playerId); // Get a reference to the player
	ytplayer.addEventListener('onStateChange','onytplayerStateChange'); // Set the function to call when the player changes states
}

// Alerts us when the youtube player changes state, i.e. goes from playing to paused, etc.function onytplayerStateChange(newState) {
function onytplayerStateChange (newState) {
	//alert("Player's new state: "+newState);
}

$(document).ready(function () {
	// Sets up Youtube player in page
	var params = { allowScriptAccess: "always" }; // Sets the player up so it can be paused, played, etc. from within our javascript
    var atts = { // Sets up how we will refer to the youtube player
    	id: "ytplayer",
    	name: "ytplayer"
    };
    swfobject.embedSWF("http://www.youtube.com/v/ibpvglbUEDE?enablejsapi=1&playerapiid=ytplayer&version=3","ytplayerelement", "425", "356", "9.0.0", null, null, { allowScriptAccess: 'always', wmode: 'transparent' }, atts); // Embeds the youtube player in the "ytplayerelement" div
});

function playPlayer (id,src) {
	if (src == 'youtube') {
		ytplayer.loadVideoById(id);
		ytplayer.playVideo();
	} else if (src == "soundcloud") {
		SC.stream("/tracks/"+id, function (sound) { // Call the Souncloud sdk function to stream a sound by the id
			scplayer = sound;
			scplayer.play();
		});
	}
}

function pausePlayer (id,src) {
	if (src == 'youtube') {
		ytplayer.pauseVideo()
	} else if (src == "soundcloud") {
		scplayer.pause();
	}
}


// Search function for sounds on Soundcloud and YouTube apps
function search () {
	$.get("/music/search?q="+$('#scsearchbox').val(), function (data) {
		console.log(data);
		$('#sclinks').empty();
		for (i=0;i<data.length;i++) {
			
			//This creates the element that holds info for particular songs
			$('#sclinks').append(
				$('<div>', {
					id: 'scsearchresult'+i,
					class: 'scsearchresult'
				}).data(data[i])
			);
			//This adds the album picture to the results
			$('#scsearchresult'+i).append(
				$('<img>', {
					id: 'scsongimage'+i,
					class: 'scsongimage',
					src: data[i].metadata.artwork
				})
			);
			//This adds text and button space
			$('#scsearchresult'+i).append(
				$('<div>', {
					id: 'sctextbuttonspace'+i,
					class: 'sctextbuttonspace',
				})
			);
			//This adds the title to the result	
			if(data[i].metadata.title.length > 50)
			{
				$("#sctextbuttonspace"+i).append(
					$("<div>", {
						id: "sctitle"+i,
						class: 'sctitle',
						text: data[i].metadata.title.substr(0,50) + "..."
					})
				);
			}else
			{
				$("#sctextbuttonspace"+i).append(
					$("<div>", {
						id: "sctitle"+i,
						class: 'sctitle',
						text: data[i].metadata.title
					})
				);
			}
			//This add a play button to the result
			$("#sctextbuttonspace"+i).append(
				$("<button>", {
					id: 'scplaybutton'+i,
					class: 'scplaybutton',
					text: "Play",
					onClick: "playPlayer ('"+data[i].id+"','"+data[i].source+"')"
				})
			);
			$("#sctextbuttonspace"+i).append(
				$("<button>", {
					id: 'scpausebutton'+i,
					class: 'scpausebutton',
					text: "Pause",
					onClick: "pausePlayer ('"+data[i].id+"','"+data[i].source+"')"
				})
			);
			//This adds a button to move a song to a playlist
			$('#sctextbuttonspace'+i).append(
				$('<button>', {
					id: 'scaddbutton'+i,
					class: 'scaddbutton',
					text: 'Add to Playlist',
					onClick: 'addToPlaylist('+i+')'
				})
			);
		}
	});
}

// Marc always makes the best comments
/* Damn straight I do */
function addToPlaylist(id) {
	songData = $("#scsearchresult"+id).data();
	al = playlistArray.length;
	playlistArray[al] = songData;
	updatePlaylistHeader();
	
	//creates visible item
	$('#list').append(
		$('<div>', {
			id: "playlistitem"+al,
			class: 'playlistitem',
		}).data(songData)
	);
	//adds text space
	$('#playlistitem'+al).append(
		$('<div>', {
			id: "playlistitemtext"+al,
			class: 'playlistitemtext',
			text: songData.metadata.title
		})
	);
	//up
	$('#playlistitem'+al).append(
		$('<div>', {
			id: "moveupbutton"+al,
			class: 'moveupbutton',
			onClick: 'moveitemup('+al+')'
		})
	);
	//down
	$('#playlistitem'+al).append(
		$('<div>', {
			id: "movedownbutton"+al,
			class: 'movedownbutton',
			onClick: 'moveitemdown('+al+')'
		})
	);
	//playFromPlaylist
	$('#playlistitem'+al).append(
		$('<div>', {
			id: "playfromplaylist"+al,
			class: 'playfromplaylist',
			onClick: "playFromPlaylist('"+songData.id+"','"+al+"')"
		})
	);
	//pauseFromPlaylist
	$('#playlistitem'+al).append(
		$('<div>', {
			id: "pauseplaylist"+al,
			class: 'pauseplaylist',
			onClick: "pausePlaylist()"
		})
	);
}

function moveitemup(pos){
	if(pos != 0)
	{
		var songToMove = playlistArray[pos];
		var holder = playlistArray[pos-1];
		
		//move item in terms of array
		playlistArray[pos-1] = songToMove;
		playlistArray[pos] = holder;
		
		//takes new array order and adds divs accordingly
		reOrder();
	}
}
function moveitemdown(pos){
	if(pos != (playlistArray.length - 1))
	{
		var songToMove = playlistArray[pos];
		var holder = playlistArray[pos+1];
		
		//move item in terms of array
		playlistArray[pos+1] = songToMove;
		playlistArray[pos] = holder;
		
		//takes new array order and adds divs accordingly
		reOrder();
	}
}

function reOrder(){
	$('#list').empty();
	for(b=0;b<playlistArray.length;b++)
	{
		//creates visible item
		$('#list').append(
			$('<div>', {
				id: "playlistitem"+b,
				class: 'playlistitem',
			}).data(playlistArray[b])
		);
		//adds text space
		$('#playlistitem'+b).append(
			$('<div>', {
				id: "playlistitemtext"+b,
				class: 'playlistitemtext',
				text: playlistArray[b].metadata.title
			})
		);
		//up
		$('#playlistitem'+b).append(
			$('<div>', {
				id: "moveupbutton"+b,
				class: 'moveupbutton',
				onClick: 'moveitemup('+b+')'
			})
		);
		//down
		$('#playlistitem'+b).append(
			$('<div>', {
				id: "movedownbutton"+b,
				class: 'movedownbutton',
				onClick: 'moveitemdown('+b+')'
			})
		);
		//playFromPlaylist
		$('#playlistitem'+b).append(
			$('<div>', {
				id: "playfromplaylist"+b,
				class: 'playfromplaylist',
				onClick: "playFromPlaylist('"+playlistArray[b].id+"','"+b+"')"
			})
		);
		//playFromPlaylist
		$('#playlistitem'+b).append(
			$('<div>', {
				id: "pauseplaylist"+b,
				class: 'pauseplaylist',
				onClick: "pausePlaylist()"
			})
		);
	}
	if(playlistState != 'play')
	{
			updatePlaylistHeader();
	}
}

function playFromPlaylist(id,ap){
	console.log(id);
	console.log(ap);
	SC.stream("/tracks/"+id,{onfinish: function(){playNext(ap);}},function (sound) {
		playlistPlayer = sound;
		playlistPosition = ap;
		updatePlaylistHeader();
		playlistState = 'play';
		playlistPlayer.play();
	});
}
function playNext(ap){
	ap++;
	if(ap < playlistArray.length)
	{
		newID = playlistArray[ap].id;
		playFromPlaylist(newID,ap);
	}
}
function pausePlaylist(){
	if(playlistState == 'play')
	{
		playlistPlayer.pause();
		playlistState = 'pause';
	}
}
function resumePlay(){}

function updatePlaylistHeader()
{	
	//get songObject
	currentSong = playlistArray[playlistPosition];
	
	//Replace album image
	$('#albumimgholder').empty();
	$('#albumimgholder').append(
			$('<img>', {
				id: "albumimg",
				src: currentSong.metadata.artwork
			})
		);
	//Replace artist
	$('#artist').empty();
	$('#artist').text(currentSong.metadata.artist);
	//Replace title
	$('#title').empty();
	$('#title').text(currentSong.metadata.title);
	
}

function createPlaylist(){
	var name = document.getElementById('playlistnamer').value;
	console.log(name);//display informaiton from form for debugging
	
	$.get("/createplaylist",{playlistname: name,playlistsongs: playlistArray});
}

function showPlaylist(){
	var name = document.getElementById('playlistsearcher').value;
	console.log(name);
	
	$.get('/showplaylist', {playlistname: name},function(passedArray){
		playlistArray = passedArray;
		playlistPosition = 0;
		reOrder();
	});
}