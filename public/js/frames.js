$(document).ready(function () {
	$("#feed").click(function () {
		$.get('/feedF', function (data) {
			document.getElementById('contentFrame').contentWindow.document.body.innerHTML = "";
			document.getElementById('contentFrame').contentWindow.document.write(data);
		});
	});
	
	$("#music").click(function () {
		$.get('/musicF', function (data) {
			document.getElementById('contentFrame').contentWindow.document.body.innerHTML = "";
			document.getElementById('contentFrame').contentWindow.document.write(data);
		});
	});

	$("#profile").click(function () {
		$.get('/profileF', function (data) {
			document.getElementById('contentFrame').contentWindow.document.body.innerHTML = "";
			document.getElementById('contentFrame').contentWindow.document.write(data);
		});
	});

});